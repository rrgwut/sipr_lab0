# SiPR - ćwiczenie 0 - Przygotowanie środowiska ROS

*Opracowane na podstawie:*

* **<https://wiki.ros.org>**
* *"Robotyka Mobilna - Laboratorium - Narzędzia wykorzystywane w programowaniu robotów mobilnych"*, Łukasz Chechliński, Daniel Koguciuk

## Wstęp

Na potrzeby ćwiczenia została przygotowana wirtualna maszyna w środowisku VirtualBox z zainstalowanym systemem Ubuntu 18.04, dostępna pod adresem: <https://wutwaw-my.sharepoint.com/:u:/g/personal/maciej_przybylski_pw_edu_pl/Eb-r2Kzx96FKsIl6n-QRTnkBHcy1xiOzlL8Vwh0PMuYbvA?e=ef55ud> (**uwaga:** do ściągnięcia plik o rozmiarze 5,6GB).

Wirtualna maszyna będzie wymagała do 20GB przestrzeni na dysku. Prędkość działania maszyny będzie zależna od wydajności komputera (RAM, CPU). W ustawieniach VirtualBox można zmienić rozmiar przydzielonego do maszyny RAM-u oraz liczbę rdzeni procesora. 

**Jeżeli posiadasz system Ubuntu** (również Kubuntu, Ubuntu Mate, Xubuntu, itp.) w wersji 18.04, możesz samodzielnie przygotować system do ćwiczenia na podstawie instrukcji: [Przygotowanie Ubuntu do pracy z ROS](doc/instalacja.md).

Istnieje również możliwość zainstalowania **ROS-a na Windows 10** (instrukcja po angielsku): <https://janbernloehr.de/2017/06/10/ros-windows>.

## Wirtualna maszyna

### Instalacja VirtualBox

Instalacja VirtualBox jest prosta - instrukcja dostępna na: <https://www.virtualbox.org/wiki/Downloads> (dla Windows jest do pobrania plik instalacyjny).

Należy zainstalować również `Extension Pack`, dostępny na: <https://www.virtualbox.org/wiki/Downloads>. Pod systemem Windows instalacja `Extension Pack`'a przebiega następująco:

![VirtualBox instalacja Extension Pack](doc/figures/virtualbox_install_extension_pack.gif)

### Dodanie wirtualnej maszyny do VirtualBox

Po ściągnięciu pliku [sipr-ubuntu-18.04.zip](https://wutwaw-my.sharepoint.com/:u:/g/personal/maciej_przybylski_pw_edu_pl/Eb-r2Kzx96FKsIl6n-QRTnkBHcy1xiOzlL8Vwh0PMuYbvA?e=ef55ud) należy jego zawartość wypakować do folderu na dysku, który będzie miał co najmniej 20GB wolnego miejsca. Pliki wirtualnej maszyny po rozpakowaniu zajmą ok 10GB, ale w trakcie pracy z maszyną ich rozmiar może się zwiększać.

Wirtualną maszynę należy dodać do programu VirtualBox.

![Dodanie wirtualnej maszyny](doc/figures/virtualbox_add_machine.gif)

### Zmiana ustawień wirtualnej maszyny

Domyślnie wirtualna maszyna wykorzystuje 4GB RAM i 1 CPU. Ustawienia te można zmienić, żeby przyspieszyć jej działanie. 

![VirtualBox ustawienia](doc/figures/virtualbox_ustawienia.gif)

### Uruchomienie wirtualnej maszyny

Kliknij przycisk Uruchom.

![VirtualBox Uruchom](doc/figures/virtualbox_uruchom.png)

Po uruchomieniu wirtualnej maszyny pojawi się ekran logowania. Należy wybrać użytkownika `sipr`. Hasło jest takie samo jak nazwa użytkownika.

![Logowanie do Ubuntu](doc/figures/virtualbox_logowanie_ubuntu.gif)

Otwórz tę instrukcję w przeglądarce Firefox wewnątrz wirtualnej maszyny wpisując adres: <https://bitbucket.org/rrgwut/sipr_lab0>

![Firefox sipr instrukcja](doc/figures/firefox_sipr_lab0.png)

## Podstawy Linuksa

ROS jest najczęściej instalowany na systemie operacyjnym Ubuntu (Linux). Praca z ROS wymaga opanowania podstaw obsługi konsoli tekstowej (terminala).

W ramach ćwiczeń będziemy korzystali z terminala `yakuake`, który uruchamia się automatycznie po starcie systemu, pozostaje jednak schowany. Jego rozwinięcie (lub ponowne schowanie) następuje po naciśnięciu klawisza **F12**.

![Terminal yakuake](doc/figures/yakuake.png)

`yakuake` wyświetlany jest na górze ekranu. Umożliwia obsługę wielu kart (pasek na dole) oraz podziały okna w pionie (`Ctrl+Shift+(`) oraz poziomie (`Ctrl+Shift+)`). Przykładowy podział przedstawia poniższy zrzut ekranu:

![Terminal yakuake - podział ekranu](doc/figures/yakuake_podzial.png)

Do poruszania się w strukturze plików wykorzystywane są następujące komendy:

| Polecenie | Opis |
| --------- | ---- |
| `ls` | Wyświetla listę plików i katalogów w obecnym folderze |
| `ls -a` | Wyświetla listę wszystkich (w tym ukrytych) plików i katalogów w obecnym folderze |
| `mkdir nowy_katalog` | Tworzy katalog o nazwie `nowy_katalog` w katalogu bieżącym |
| `cd` | Change directory – zmiana katalogu bieżącego. Przy braku argumentów oznacza powrót do katalogu domowego użytkownika. |
| `cd ~` | Przejście do katalogu domowego użytkownika | 
| `cd ./Dokumenty` | Przejście do katalogu `Dokumenty` (o ile taki katalog znajduje się w katalogu bieżącym |
| `cd ..` | Przejście do katalogu nadrzędnego 
| `pwd` | Print working directory – wypisanie pełnej ścieżki bieżącego katalogu roboczego. |

W terminalu można również uruchamiać programy posiadające interfejs graficzny. W ten sposób uruchamiana będzie również większość narzędzi wykorzystywanych przy pracy z robotami.

## Tworzenie przestrzeni roboczej ROS

> **UWAGA!**
>
> Przed rozpoczęciem pracy z wirtualną maszyną należy zaktualizować system poleceniami:
>
> ```
> sudo apt update
> ```
>
> ```
> sudo apt upgrade
> ```


0. Sprawdź czy jest załadowana główna przestrzeń robocza ROS-a:

    ```
    roscd
    ```

    powinno przenieść nas do katalogu `/opt/ros/melodic`:

    ![roscd](doc/figures/roscd.png)

1. Przejdź do katalogu domowego użytkownika:

    ```
    cd
    ```

2. Stwórz katalogi własnej przestrzeni roboczej

    ```
    mkdir -p sipr_ws/src
    ```

    ![mkdir](doc/figures/mkdir.png)

3. Wejdź do katalogu `~/sipr_ws/src` i zainicjuj przestrzeń roboczą

    ```
    cd sipr_ws/src
    ```

    ```
    catkin_init_workspace
    ```

4. Wejdź do katalogu `~/sipr_ws` (jeden poziom wyżej) i zbuduj przestrzeń roboczą

    ```
    cd ..
    ```

    ```
    catkin_make
    ```

    Po zbudowaniu przestrzeni roboczej będzie ona zawierać trzy katalogi. Wpisz polecenie:

    ```
    tree ~/sipr_ws -L 1
    ```

    ![Drzewo katalogów](doc/figures/workspace_tree.png)

5. W każdej nowej sesji terminala należy załadować naszą przestrzeń roboczą poleceniem:

    ```
    source ~/sipr_ws/devel/setup.bash
    ```

    Aby nie musieć powtarzać ładowania można dodać tę instrukcję do pliku `~/.bashrc`, który jest uruchamiany przy otwarciu nowej sesji terminala.

    ```
    echo "source ~/sipr_ws/devel/setup.bash" >> ~/.bashrc
    ```

6. Teraz `roscd` przenosić nas będzie do katalogu `~/sipr_ws/devel/`

    ```
    roscd
    ```

    ![roscd](doc/figures/roscd_2.png)
