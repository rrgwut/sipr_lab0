# Przygotowanie Windowsa 10 do pracy z ROS

## Instalacja środowiska programistycznego Visual Studio Code

<https://code.visualstudio.com/>

## Instalacja oprogramowania pomocniczego

### Git (wersjonowanie treści)

<https://git-scm.com/download/win>

Jako domyślny edytor można wybrać VS Code: 

![Instalacja Git](figures/git-install-default-editor.png)
