# Przygotowanie Ubuntu do pracy z ROS

W tej sekcji wyjaśnione jest krok po kroku **jak przygotować świeży system Ubuntu 18.04 do ćwiczeń**. Poniższe polecenia należy uruchomić w konsoli. 

## Uruchomienie wbudowanego terminala

![Ubuntu terminal](figures/ubuntu-wbudowany-terminal.gif)

## Aktualizacja systemu

*Wpisz poniższe instrukcje w terminalu i zatwierdź Enterem.*

1. Zebranie informacji o aktualizacjach

    ```bash
    sudo apt update
    ```

    ![apt update](figures/apt-update.gif)

2. Instalacja aktualizacji

    ```bash
    sudo apt upgrade
    ```

## Instalacja oprogramowania pomocniczego

1. Konsola rozwijana `yakuake`

    ```bash
    sudo apt install yakuake
    ```

    Dodaj `yakuake` do programów uruchamianych na starcie:
    ![Dodaj `yakuake` do startowych](figures/dodaj-start-yakuake.gif)

    Przeloguj się:
    ![Przelogowanie](figures/ubuntu-wyloguj.gif)

    Wciśnij **F12** i potwierdź domyślne ustawienia.
    ![`yakuake` pierwsze uruchomienie](figures/yakuake-pierwsze-uruchomienie.gif)

2. Oprogramowanie do wersjonowania treści (powoli ściągnąć kody źródłowe gotowych projektów)

    ```bash
    sudo apt install git
    ```

3. Inne przydatne Oprogramowanie

    ```bash
    sudo apt install tree
    ```

## Instalacja ROS-a

*Oryginalna instrukcja: <https://wiki.ros.org/kinetic/Installation/Ubuntu>*

Konfiguracja repozytorium, z którego będą pobierane pakiety ROS-a:

```bash
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
```

```bash
sudo apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
```

```bash
sudo apt-get update
```

Właściwa instalacja ROS-a w wersji **melodic** (może zająć kilkanaście minut, do ściągnięcia ok. 2GB):

```bash
sudo apt-get install ros-melodic-desktop-full
```

### Konfiguracja konsoli

Aby skorzystać z poleceń ROS-a należy w danej sesji terminala załadować główną przestrzeń roboczą ROS-a:

```bash
source /opt/ros/melodic/setup.bash
```

Powyższe polecenie należy wywoływać w każdej nowej sesji terminala. Aby tego uniknąć można należy to polecenie dodać na stałe do pliku `~/.bashrc`:

```bash
echo "source /opt/ros/melodic/setup.bash" >> ~/.bashrc
```

```bash
source ~/.bashrc
```

### Dodatkowe oprogramowanie do ROS-a

```bash
sudo apt install python-rosdep python-rosinstall python-rosinstall-generator python-wstool build-essential
```

```bash
sudo rosdep init
rosdep update
```

## Instalacja środowiska programistycznego Visual Studio Code

```bash
sudo snap install code --classic
```

### Instalacja dodatków do VS Code

```bash
code --install-extension ms-iot.vscode-ros
```
